Name:       qbootctl
Version:    0.2.0
Release:    1%{?dist}
Summary:    A systemd service controller for multi-nodes environments
License:    GPL-3.0-or-later AND GPL-2.0-or-later AND Apache-2.0 AND BSD-3-Clause

# Consider changing to gitlab.com/sdm845-mainline/qbootctl when upstreamed
URL:        https://github.com/ericcurtin/%{name}
Source0:    %{url}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  meson

%description
This is an open source equivalent of abctl for SDM845 based devices, including
Qualcomm Automotive boards.

%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%{_bindir}/qbootctl

%changelog
* Wed Jun 28 2023 Eric Curtin <ecurtin@redhat.com> - 0.2.0-1
- Initial release

